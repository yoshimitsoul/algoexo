package tortuedego;

public class TortueGo {
	
		// les données privées
		private int _posX, _posY ;
		private Direction _dir;
		
		
		
		//méthodes publiques
		public TortueGo() { //position initiale ou constructeur par defaut
		this._posX = 0;
		this._posY = 0;
		this._dir = Direction.Nord;
		}
		

		public TortueGo(int posX0, int posY0, Direction dir) {
		this._posX  = posX0;
		this._posY = posY0;
		this._dir = dir;
	}
	//pseudo accesseur	
	public void ouEsTu( ) {
		System.out.println("je suis en " + this._posX + " , " + this._posY + " et je me dirige en position : " + this._dir);
	}
	
	public void avance () {
		switch(this._dir) {
		case Nord:
			this._posX +=1;
			break;
		case Sud:
			this._posY -=1;
			break;
		case Est: 
			this._posX +=1;
			break;
		case Ouest:
			this._posX -=1;
		break;
			
		}
	}
	
	public void tourneAdroite () {
		switch(this._dir) {
		case Nord:
			this._dir = Direction.Est;
			break;
		case Sud:
			this._dir = Direction.Ouest;
			break;
		case Est: 
			this._dir = Direction.Sud;
			break;
		case Ouest:
			this._dir = Direction.Nord;
		break;
			
		}
	}
	
	
	public void tourneAgauche () {
		switch(this._dir) {
		case Nord:
			this._dir = Direction.Ouest;
			break;
		case Sud:
			this._dir = Direction.Est;
			break;
		case Est: 
			this._dir = Direction.Nord;
			break;
		case Ouest:
			this._dir = Direction.Sud;
		break;
			
		}
	}
	public Direction getDirection() {
		return this._dir;
	}
	
		
}
