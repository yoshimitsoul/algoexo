package tortuedego;

public class mainTortue {

	public static void main(String[] args) {

		TortueGo franklin, bob;
		Direction x;
		x = Direction.Ouest;

		franklin = new TortueGo();
		bob = new TortueGo(10, 15, Direction.Ouest);
		bob.ouEsTu();
		bob.avance();
		bob.ouEsTu();
		bob.tourneAdroite();
		bob.ouEsTu();
		bob.tourneAgauche();
		bob.ouEsTu();
		bob.getDirection();
		bob.ouEsTu();

	}

}
